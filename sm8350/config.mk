PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/sm8350

PRODUCT_COPY_FILES += \
    vendor/oneplus/apps/sm8350/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/oneplus/apps/sm8350/proprietary/system/framework/oneplus-framework.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/oneplus-framework.jar \
    vendor/oneplus/apps/sm8350/proprietary/system/framework/oneplus-services.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/oneplus-services.jar \
    vendor/oneplus/apps/sm8350/proprietary/system_ext/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.camera.xml \
    vendor/oneplus/apps/sm8350/proprietary/system_ext/etc/configs/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml \
    vendor/oneplus/apps/sm8350/proprietary/system_ext/etc/permissions/com.oneplus.gallery.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.gallery.xml \
    vendor/oneplus/apps/sm8350/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skl.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/bin/hw/vendor.oneplus.hardware.CameraMDMHIDL@1.0-service:$(TARGET_COPY_OUT_VENDOR)/bin/hw/vendor.oneplus.hardware.CameraMDMHIDL@1.0-service \
    vendor/oneplus/apps/sm8350/proprietary/vendor/bin/hw/vendor.oneplus.hardware.charger@1.0-service:$(TARGET_COPY_OUT_VENDOR)/bin/hw/vendor.oneplus.hardware.charger@1.0-service \
    vendor/oneplus/apps/sm8350/proprietary/vendor/etc/init/vendor.oneplus.hardware.CameraMDMHIDL@1.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.oneplus.hardware.CameraMDMHIDL@1.0-service.rc \
    vendor/oneplus/apps/sm8350/proprietary/vendor/etc/init/vendor.oneplus.hardware.charger@1.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.oneplus.hardware.charger@1.0-service.rc \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/libalgo.facebeauty.so:$(TARGET_COPY_OUT_VENDOR)/lib/libalgo.facebeauty.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/libFaceBeautyCap.so:$(TARGET_COPY_OUT_VENDOR)/lib/libFaceBeautyCap.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/libFaceBeautyPre.so:$(TARGET_COPY_OUT_VENDOR)/lib/libFaceBeautyPre.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/libPreviewAlgo.prefacebeauty.so:$(TARGET_COPY_OUT_VENDOR)/lib/libPreviewAlgo.prefacebeauty.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/vendor.oneplus.hardware.charger@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib/vendor.oneplus.hardware.charger@1.0.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib/hw/camera.qcom.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/camera.qcom.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/libalgo.facebeauty.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libalgo.facebeauty.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/libFaceBeautyCap.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libFaceBeautyCap.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/libFaceBeautyPre.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libFaceBeautyPre.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/libPreviewAlgo.prefacebeauty.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libPreviewAlgo.prefacebeauty.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/vendor.oneplus.hardware.charger@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.oneplus.hardware.charger@1.0.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/vendor.oneplus.hardware.CameraMDMHIDL@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.oneplus.hardware.CameraMDMHIDL@1.0.so \
    vendor/oneplus/apps/sm8350/proprietary/vendor/lib64/hw/camera.qcom.so:$(TARGET_COPY_OUT_VENDOR)/lib64/hw/camera.qcom.so

PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService \
    OnePlusGallery \
